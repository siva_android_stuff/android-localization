package com.siva.practice.localization.helper;

import android.content.Context;
import android.content.res.Configuration;

import com.siva.practice.localization.enums.Locale;

/**
 * Created by ksees_000 on 4/25/2016.
 */
public class LocaleHelper {

    public static void changeLocale(Context context, Locale locale) {

//        java.util.Locale locale1;
//        switch (locale) {
//            case TELUGU:
//                locale1 = new java.util.Locale("te");
//                break;
//            default:
//                locale1 = new java.util.Locale("en");
//        }

        java.util.Locale.setDefault(locale.getLocale());
        Configuration config = context.getResources().getConfiguration();
        config.locale = locale.getLocale();
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }
}
