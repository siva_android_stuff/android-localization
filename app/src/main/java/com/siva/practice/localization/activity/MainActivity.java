package com.siva.practice.localization.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import com.siva.practice.R;
import com.siva.practice.localization.enums.Locale;
import com.siva.practice.localization.helper.LocaleHelper;
import com.siva.practice.preferences.activity.SettingsActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.my_toolbar)
    Toolbar myToolbar;

    @Bind(R.id.textcontent)
    TextView textContentTextView;

    // This TAG will be common code for all the classes. This is the name used for logging.
    private String TAG = this.getClass().getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(myToolbar);
        ButterKnife.bind(this);
        myToolbar.inflateMenu(R.menu.actionbarmenu);
        myToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.settings) {
                    Intent i = new Intent(getBaseContext(), SettingsActivity.class);
                    startActivity(i);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeLocale();
    }

    /**
     * When we switch from the main activity to the settigs screen, The main activity is paused.
     * And once we switch back, main activity will resume. This way, the activity will resume and invoke the onResume method of its lifecycle.
     */
    @Override
    protected void onResume() {
        super.onResume();
        changeLocale();
    }

    /**
     * This is a common method is used to change/refresh the locale of the application.
     */
    private void changeLocale() {
        Log.d(TAG, "Changing Locale");
        String lang = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("lang", "en");
        LocaleHelper.changeLocale(getApplicationContext(), Locale.getLocaleFromString(lang));
        refreshViewText();
        Log.d(TAG, "Changed Locale");
    }

    /**
     * This method is used to reset the lanaguage to adjust to the new locale.
     * It is because, when we have changed the locale, The change would be applying to new Activity but not the current one.
     * http://www.sureshjoshi.com/mobile/changing-android-locale-programmatically/
     */
    public void refreshViewText() {
        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(1000);
        textContentTextView.startAnimation(in);
        textContentTextView.setText(R.string.story);

        myToolbar.setTitle(R.string.app_name);
        myToolbar.setTitleTextColor(getResources().getColor(R.color.textColor));
    }
}
