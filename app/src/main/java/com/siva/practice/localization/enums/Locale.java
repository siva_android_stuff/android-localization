package com.siva.practice.localization.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by ksees_000 on 4/25/2016.
 */
public enum Locale {
    TELUGU("te", new java.util.Locale("te")),
    ENGLISH("en", new java.util.Locale("en"));

    private String language;
    private java.util.Locale locale;

    Locale(String language, java.util.Locale locale) {
        this.language = language;
        this.locale = locale;
    }

    public static Locale getLocaleFromString(String language) {
        for (Locale locale :
                Locale.values()) {
            if (StringUtils.equalsIgnoreCase(locale.getLanguage(), language)) {
                return locale;
            }
        }
        return null;
    }

    public String getLanguage() {
        return language;
    }

    public java.util.Locale getLocale() {
        return locale;
    }
}
