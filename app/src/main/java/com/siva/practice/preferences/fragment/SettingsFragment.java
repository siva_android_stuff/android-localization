package com.siva.practice.preferences.fragment;

import android.os.Bundle;

import com.siva.practice.R;

public class SettingsFragment extends android.preference.PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
