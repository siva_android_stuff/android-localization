package com.siva.practice.preferences.preference;

import android.content.Context;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by ksees_000 on 4/25/2016.
 */
public class LanguageListPreference extends ListPreference {

    private static String TAG = LanguageListPreference.class.getCanonicalName();

    public LanguageListPreference(final Context context) {
        this(context, null);
    }

    public LanguageListPreference(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public CharSequence getSummary() {

        String lang = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("lang", "en");
        final CharSequence entry = getEntryFromLang(lang);
        final CharSequence summary = super.getSummary();
        if (summary == null || entry == null) {
            return null;
        } else {
            return entry.toString();
        }
    }

    private CharSequence getEntryFromLang(String lang) {
        if (StringUtils.equalsIgnoreCase(lang, "te")) {
            return "Telugu";
        } else {
            return "English";
        }
    }

    @Override
    public void setValue(final String value) {
        super.setValue(value);
        notifyChanged();
    }
}
